#include <stdio.h>
#include<iostream>

int main()
{
  int a, n, p, q, c, d, k, suma = 0;
  int principala[10][10], secundara[10][10], rezultat[10][10];

  printf("Introdu numarul de coloane si randuri a matricei principale\n");
  scanf("%d%d", &a, &n);
  printf("Introdu numarul de elemente a primei matrice\n");

  for (c = 0; c < a; c++)
    for (d = 0; d < n; d++)
      scanf("%d", &principala[c][d]);

  printf("Introdu numarul de coloane si randuri a matricei secundare\n");
  scanf("%d%d", &p, &q);

  if (n != p)
    printf("Matricea nu poate fi multiplicata\n");
  else
  {
    printf("Introdu elementele matricei secunde\n");

    for (c = 0; c < p; c++)
      for (d = 0; d < q; d++)
        scanf("%d", &secundara[c][d]);

    for (c = 0; c < a; c++) {
      for (d = 0; d < q; d++) {
        for (k = 0; k < p; k++) {
          suma = suma + principala[c][k]*secundara[k][d];
        }

        rezultat[c][d] = suma;
        suma = 0;
      }
    }

    printf("Matricea rezultata:\n");

    for (c = 0; c < a; c++) {
      for (d = 0; d < q; d++)
        printf("%d\t", rezultat[c][d]);

      printf("\n");
    }
  }

  return 0;
}
